const mongoose = require("mongoose");

const User = require("./User.model");

const connection = "mongodb://mongo:27017/mongotest";

const connectDb = () => {

  return mongoose.connect(connection, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true, useFindAndModify: false })

};

module.exports = connectDb;
